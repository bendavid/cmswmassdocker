#!/bin/bash
_sitepackages=$1
find "/usr/lib/root" -maxdepth 1 -mindepth 1 \( -iname '*py*' -or -name '*Js*' -or -name 'ROOT' -or -name 'DistRDF' \) \
        ! \( -name '*EGPythia8*' -or -iname '*.rootmap' -or -iname '*.pcm' \) -print0 | while read -rd $'\0' _lib; do \
    _base=$(basename "${_lib}"); \
    ln -sf "/usr/lib/root/${_base}" "${_sitepackages}/${_base}"; \
    done
