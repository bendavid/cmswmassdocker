# cmswmassdocker

build with
 ```
 podman build --ulimit nofile=262144:262144 --format docker -t wmassdevrolling build
 ```

or for patch releases build with
 ```
 podman build --ulimit nofile=262144:262144 --format docker -t wmassdevrolling build_patch
 ```
 
 push with
 ```
 podman login gitlab-registry.cern.ch
 podman push wmassdevrolling gitlab-registry.cern.ch/bendavid/cmswmassdocker/wmassdevrolling
```
